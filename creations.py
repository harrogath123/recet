import mysql.connector
from unidecode import unidecode

# connect à mysql
connect = mysql.connector.connect(
    host='localhost',
    user='benjamin',
    password='coucou',
    database='data'
)
curse = connect.cursor(dictionary=True)

def normalize(l):
    return unidecode(" ".join([t[:-1] if t.endswith("s") else t for t in l.lower().split(' ')]))

# ouvre le fichier recette.csv
with open('recettes.csv') as csv:
    # pour chaque ligne
    for line in csv.readlines():
        entre = line.split(",")  
        # recuperer le nom de la recette
        nom_recette = normalize(entre[2]).replace('"', '').strip()
        print(nom_recette)

        # recup la qt de l'ingredient
        try: 
            qt = int(entre[1])
        except:
            continue
        
        if qt < 0:
            qt = 0

        # → si elle dans la bdd, je recup id
        curse.execute('SELECT id FROM Recette WHERE nom_recette LIKE %s', [nom_recette])
        if result := curse.fetchone():
            id_recette = result["id"]
            #   sinon je l'ajoute
        else:
            curse.execute('INSERT INTO Recette(nom_recette, description) VALUES (%s, %s)', [nom_recette, ""])
            id_recette = curse.lastrowid

        # recuperer le nom de l'ingredient
        nom_ingredient = normalize(entre[0]).replace('"', '').strip()
        # id de l'ingredient
        curse.execute('SELECT id FROM Produit WHERE nom_produit LIKE %s', [nom_ingredient])
        if result := curse.fetchone():
            id_prod = result["id"]
        else:
            curse.execute('INSERT INTO Produit(nom_produit) VALUES (%s)', [nom_ingredient])
            id_prod = curse.lastrowid

        # → ajouter dans la table Ingredient
curse.execute('INSERT INTO Ingredient (nom_ingredient) VALUES (%s)', [nom_ingredient])
id_prod = curse.lastrowid

# → ajouter dans la table RecetteIngredient
curse.execute('INSERT INTO RecetteIngredient (recette_id, ingredient_id, quantite) VALUES (%s, %s, %s)', [id_recette, id_prod, qt])

# commit de la bdd !
connect.commit()

