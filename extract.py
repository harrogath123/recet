import pandas as pd
import mysql.connector

# Configurations de connexion à la base de données
db_config = {
    "host": "localhost",
    "user": "benjamin",
    "password": "coucou",
    "database": "data"
}

# Connexion à la base de données
cnx = mysql.connector.connect(**db_config)
cursor = cnx.cursor()

try:
    # Ajouter une recette de tarte aux pommes
    cursor.execute("INSERT INTO Recette (nom_recette, description) VALUES (%s, %s)", ('Tarte aux Pommes', 'Une délicieuse tarte aux pommes'))
    
    # Récupérer l'id de la recette nouvellement insérée
    cursor.execute("SELECT LAST_INSERT_ID() AS id")
    recette_id = cursor.fetchone()[0]

    # Ajouter les ingrédients à la recette
    ingredients = [
        ('Farine', 200),
        ('Œufs', 3),
        ('Pommes', 4),
        ('Sucre', 150)
    ]

    for nom_ingredient, quantite in ingredients:
        # Ajouter l'ingrédient s'il n'existe pas
        cursor.execute("INSERT IGNORE INTO Ingredient (nom_ingredient) VALUES (%s)", (nom_ingredient,))
        # Récupérer ou insérer l'ingrédient
        cursor.execute("SELECT id FROM Ingredient WHERE nom_ingredient = %s", (nom_ingredient,))
        ingredient_id = cursor.fetchone()[0]
        # Associer l'ingrédient à la recette avec une quantité
        cursor.execute("INSERT INTO RecetteIngredient (recette_id, ingredient_id, quantite) VALUES (%s, %s, %s)", (recette_id, ingredient_id, quantite))

    # Committer les changements
    cnx.commit()
    print("Recette de Tarte aux Pommes ajoutée avec succès!")

    # Créer un DataFrame avec les informations
    data = {
        "Code Python": ["import pandas as pd", "import mysql.connector", " ... "],
        "Code SQL": ["DROP DATABASE IF EXISTS data; CREATE DATABASE IF NOT EXISTS data; ...", "INSERT INTO Recette ...", "INSERT INTO Ingredient ..."],
        "Autres Informations": ["Nom de la recette: Tarte aux Pommes", "Ingrédients: Farine (200 g), Œufs (3), Pommes (4), Sucre (150 g)"]
    }

    # Trouver la longueur maximale parmi les listes dans le dictionnaire
    max_length = max(len(lst) for lst in data.values())

    # Ajouter des éléments vides pour égaliser la longueur
    for key in data:
        data[key] += [''] * (max_length - len(data[key]))

    df = pd.DataFrame(data)

    # Sauvegarder le DataFrame dans un fichier CSV
    df.to_csv('informations_code.csv', index=False)

    print("Fichier CSV créé avec succès!")

except mysql.connector.Error as err:
    print(f"Erreur : {err}")

finally:
    # Fermer la connexion
    cursor.close()
    cnx.close()
