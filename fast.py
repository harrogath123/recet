from fastapi import FastAPI, HTTPException
from pydantic import BaseModel
import mysql.connector
from pydantic import BaseModel, ValidationError
# Correction : Utilisez un dictionnaire pour spécifier les paramètres de connexion
mydb = mysql.connector.connect(
    host="localhost",
    user="benjamin",
    password="coucou",
    database="BDD_REC"
)

app = FastAPI()

class Recette(BaseModel):
    nom: str
    Quantite: int
    product_id: str

class product(BaseModel):
    nom: str
    Quantite: int
    product_id: str

class ingredients (BaseModel):
    nom:str
    recette:str
    Quantite: int

@app.get("/produit")
def get_recette():
    with mydb.cursor(dictionary=True) as cursor:
        cursor.execute("SELECT * FROM product")
        result = cursor.fetchall()
    return result

# Créer un nouveau produit

@app.post("/produit")
def get_produit(recette:product):
    try:
        with mydb.cursor(dictionary=True) as cursor:
            cursor.execute("""
                INSERT INTO recettes (nom, Quantite, product_id) VALUES (%(nom)s, %(Quantite)s, %(product_id)s);
            """, {"nom": recette.nom, "Quantite": recette.Quantite, "product_id": recette.product_id})
            mydb.commit()
            result = cursor.lastrowid
            return {"result": result, "nom": recette.nom,"Quantite":recette.Quantite,"product_id":recette.product_id}
    except mysql.connector.Error as err:
        print("Erreur MySQL : {}".format(err))
        raise HTTPException(status_code=500, detail="Erreur lors de l'exécution de la requête MySQL : {}".format(err))


@app.post("/recette")
def ajouter_recette(recette: Recette):
    try:
        # Vérifier la connexion à la base de données
        if not mydb.is_connected():
            mydb.reconnect()

        with mydb.cursor(dictionary=True) as cursor:
            # Ajouter la recette avec la quantité
            cursor.execute("""
                INSERT INTO recettes (nom, Quantite, product_id) 
                VALUES (%(nom)s, %(quantite)s, %(product_id)s);
            """, {
                "nom": recette.nom,
                "quantite": recette.Quantite,
                "product_id": recette.product_id
            })

            mydb.commit()

            return {"result": "Recette ajoutée", "nom": recette.nom,"quantite":recette.Quantite,"product_id":recette.product_id}
    except Exception as e:
        print(f"Erreur lors de l'ajout de la recette : {e}")
        raise HTTPException(status_code=500, detail=f"Erreur interne du serveur : {e}")

   
@app.put("/stock")
def modifier_stock(product: product):
    try:
        # Vérifier la connexion à la base de données
        if not mydb.is_connected():
            mydb.reconnect()

        with mydb.cursor(dictionary=True) as cursor:
            # Récupérer le stock actuel du produit
            cursor.execute("SELECT Quantite FROM recettes WHERE nom = %s", (product.nom,))
            stock_actuel = cursor.fetchone()

            if stock_actuel is None:
                raise HTTPException(status_code=404, detail=f"Le produit avec le nom '{product.nom}' n'a pas été trouvé.")

            nouveau_stock = stock_actuel["Quantite"] + product.Quantite

            # Mettre à jour le stock du produit
            cursor.execute("UPDATE recettes SET Quantite = %s WHERE nom = %s", (nouveau_stock, product.nom))

            mydb.commit()

            return {"result": f"Stock du produit '{product.nom}' mis à jour", "nouveau_stock": nouveau_stock}
    except Exception as e:
        print(f"Erreur lors de la modification du stock : {e}")
        raise HTTPException(status_code=500, detail=f"Erreur interne du serveur : {e}")

    
    



