-- Création des tables

DROP DATABASE IF EXISTS BDD_REC;
CREATE DATABASE BDD_REC;
USE BDD_REC;

CREATE TABLE IF NOT EXISTS restaurant1 (
  Nom VARCHAR(50) PRIMARY KEY,
  Quantite INT,
  product_id VARCHAR(50),
  Date DATE,
  restaurant_id INT
);

CREATE TABLE IF NOT EXISTS restaurant2 (
  Nom VARCHAR(50) PRIMARY KEY,
  Quantite INT,
  product_id varchar(50),
  Date DATE,
  restaurant_id INT
);

CREATE TABLE IF NOT EXISTS restaurant3 (
  Nom VARCHAR(50) PRIMARY KEY,
  Quantite INT,
  product_id VARCHAR,
  restaurant_id INT
);

CREATE TABLE IF NOT EXISTS recettes (
  nom VARCHAR(50) PRIMARY KEY,
  Quantite INT,
  product_id varchar(50)
  ingredients varchar(50)
);

CREATE TABLE IF NOT EXISTS ingredients (
  nom VARCHAR(50) PRIMARY KEY,
  recette varchar(50),
  quantite INT,
  Date DATE
);

CREATE TABLE If NOT EXISTS product(
  nom VARCHAR (50) PRIMARY KEY,
  Quantite INT,
  product_id varchar (50)
);

CREATE TABLE IF NOT EXISTS ingredients_recette (
    id INT AUTO_INCREMENT PRIMARY KEY,
    recette_id INT,
    ingredient VARCHAR(255),
    Quantite INT,
    FOREIGN KEY (recette_id) REFERENCES recettes(id)
);


