
DROP DATABASE IF EXISTS data;
CREATE DATABASE IF NOT EXISTS data;
USE data;

-- Table pour les produits
CREATE TABLE IF NOT EXISTS Produit (
    id INT AUTO_INCREMENT PRIMARY KEY,
    nom_produit VARCHAR(255)
);

-- Table pour les restaurants
CREATE TABLE IF NOT EXISTS Restaurant (
    id INT AUTO_INCREMENT PRIMARY KEY,
    nom_restaurant VARCHAR(255)
);


-- Table pour le stock des produits dans les restaurants
CREATE TABLE IF NOT EXISTS StockProduit (
    produit_id INT,
    restaurant_id INT,
    quantite INT,
    PRIMARY KEY (produit_id, restaurant_id),
    FOREIGN KEY (produit_id) REFERENCES Produit(id),
    FOREIGN KEY (restaurant_id) REFERENCES Restaurant(id)
);

-- Table pour les recettes
CREATE TABLE IF NOT EXISTS Recette (
    id INT AUTO_INCREMENT PRIMARY KEY,
    nom_recette VARCHAR(255),
    description VARCHAR(255)
);

-- Table pour les ingrédients des recettes
CREATE TABLE IF NOT EXISTS Ingredient (
    id INT AUTO_INCREMENT PRIMARY KEY,
    nom_ingredient VARCHAR(255)
);

-- Table de liaison entre Recette et Ingredient
CREATE TABLE IF NOT EXISTS RecetteIngredient (
    recette_id INT,
    ingredient_id INT,
    quantite INT,
    PRIMARY KEY (recette_id, ingredient_id),
    FOREIGN KEY (recette_id) REFERENCES Recette(id),
    FOREIGN KEY (ingredient_id) REFERENCES Ingredient(id)
);

grant all privileges on data.* to benjamin@localhost;