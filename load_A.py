import pandas as pd #importer pandas
import mysql.connector #import mysql pour connecter le fichier mysqlconnector avec celui de la database
from mysql.connector import errorcode
import re  # Importation du module 're' pour effectuer des opérations sur les expressions régulières

# Configurations de connexion à la base de données
db_config = {
    "host": "localhost", #hote
    "user": "benjamin", #utilisateur
    "password": "coucou",#mot de passe
    "database": "BDD_REC"#base de données d'init.sql
}

# Connexion à MySQL en utilisant les paramètres de configuration définis précédemment
cnx = mysql.connector.connect(**db_config)  # Connexion à MySQL en utilisant les informations de connexion dans db_config

# Création d'un objet curseur pour exécuter des requêtes SQL
cursor = cnx.cursor()  # Création d'un objet curseur pour interagir avec la base de données

# Charger les données du fichier CSV dans un DataFrame pandas
csv_file_path = '/home/benjamin/Recettes/grouped_extract_A.csv'
df = pd.read_csv(csv_file_path)
df['product'] = df['product'].apply(lambda x: re.sub(r'[^a-zA-Z0-9\s]', '', str(x)))

# Afficher les noms des colonnes
print(df.columns)

# Connexion à la base de données MySQL
try:
    # Établir une connexion à la base de données et créer un objet curseur 
    #Un curseur est un pointeur qui parcourt un ensemble de tuples dans un certain ordre prédéterminé par le système de bases de données
    cnx = mysql.connector.connect(**db_config)
    cursor = cnx.cursor()

    # Parcourir chaque ligne du DataFrame
    for index, row in df.iterrows():
        # Construire la requête SQL d'insertion avec mise à jour en cas de clé déjà existante
        query = f"INSERT INTO restaurant1(Nom, Quantite) VALUES ('{row['product']}', {row['stock']}) ON DUPLICATE KEY UPDATE Quantite = {row['stock']}"

        # Exécuter la requête SQL
        cursor.execute(query)

    # Valider les modifications dans la base de données
    cnx.commit()

except mysql.connector.Error as err:
    # Gérer les erreurs liées à la base de données
    print(f"Erreur : {err}")

finally:
    # Fermer le curseur et la connexion, si la connexion est active
    if 'cnx' in locals() and cnx.is_connected():
        cursor.close()
        cnx.close()




