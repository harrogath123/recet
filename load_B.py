import pandas as pd
import mysql.connector
from mysql.connector import errorcode
import re
from datetime import datetime

# Configurations de connexion à la base de données
db_config = {
    "host": "localhost",
    "user": "benjamin",
    "password": "coucou",
    "database": "BDD_REC"
}

# Charger les données du fichier CSV dans un DataFrame pandas
csv_file_path = '/home/benjamin/Recettes/grouped_extract_B.csv'
df = pd.read_csv(csv_file_path)
df['stock'] = df['stock'].apply(lambda x: re.sub(r'[^a-zA-Z0-9\s]', '', str(x)))

# Connexion à la base de données MySQL
try:
    cnx = mysql.connector.connect(**db_config)
    cursor = cnx.cursor()

    # Ajouter les données dans la table restaurant2
    for index, row in df.iterrows():
        # Entourer la date avec des guillemets simples
        date_value = datetime.strptime(row['date'], '%Y-%m-%d').strftime('%Y-%m-%d')
        query = f"INSERT INTO restaurant2 (Nom, Quantite, Date) VALUES ('{row['stock']}', {row['inventory']}, '{date_value}') ON DUPLICATE KEY UPDATE Quantite = {row['inventory']}, Date = '{date_value}'"

        cursor.execute(query)

    # Valider et fermer la transaction
    cnx.commit()

except mysql.connector.Error as err:
    print(f"Erreur : {err}")

finally:
    # Fermer la connexion à la base de données
    if 'cnx' in locals() and cnx.is_connected():
        cursor.close()
        cnx.close()
