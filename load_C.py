import pandas as pd
import mysql.connector
from mysql.connector import errorcode
import re

# Configurations de connexion à la base de données
db_config = {
    "host": "localhost",
    "user": "benjamin",
    "password": "coucou",
    "database": "BDD_REC"
}

cnx = mysql.connector.connect(**db_config)
cursor = cnx.cursor()

# Charger les données du fichier CSV dans un DataFrame pandas
csv_file_path = '/home/benjamin/Recettes/grouped_extract_C.csv'
df = pd.read_csv(csv_file_path)
df['product'] = df['product'].apply(lambda x: re.sub(r'[^a-zA-Z0-9\s]', '', str(x)))

# Connexion à la base de données MySQL
try:
    cnx = mysql.connector.connect(**db_config)
    cursor = cnx.cursor()

    # Ajouter les données dans la table restaurant3
    for index, row in df.iterrows():
        check_query = f"SELECT * FROM restaurant3 WHERE Nom = '{row['product']}'"
        cursor.execute(check_query)
        result = cursor.fetchone()

        if result:
            # La valeur existe déjà, mettez à jour la quantité
            update_query = f"UPDATE restaurant3 SET Quantite = {row['stock']} WHERE Nom = '{row['product']}'"
            cursor.execute(update_query)
        else:
            # La valeur n'existe pas, insérez une nouvelle ligne
            insert_query = f"INSERT INTO restaurant3(Nom, Quantite) VALUES ('{row['product']}', {row['stock']})"
            cursor.execute(insert_query)

    # Valider et fermer la transaction
    cnx.commit()

except mysql.connector.Error as err:
    print(f"Erreur : {err}")

finally:
    # Fermer la connexion à la base de données
    if 'cnx' in locals() and cnx.is_connected():
        cursor.close()
        cnx.close()
