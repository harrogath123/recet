from fastapi import FastAPI, HTTPException, Depends
from pydantic import BaseModel
import mysql.connector
from mysql.connector import pooling

app = FastAPI()

# Configuration de la connexion à la base de données
db_config = {
    "host": "localhost",
    "user": "benjamin",
    "password": "coucou",
    "database": "data",
    "pool_name": "recette_pool",
    "pool_size": 5
}

# Pool de connexions à la base de données
db_pool = mysql.connector.pooling.MySQLConnectionPool(**db_config)

# Fonction pour obtenir la connexion à la base de données depuis le pool
def get_db():
    return db_pool.get_connection()

class RecetteIngredient(BaseModel):
    nom_ingredient: str
    quantite: int

class Recette(BaseModel):
    nom_recette: str
    ingredients: list[RecetteIngredient]
    description: str

@app.post("/recette")
def ajouter_recette(recette: Recette, db: mysql.connector.connection.MySQLConnection = Depends(get_db)):
    try:
        # Créer un curseur
        cursor = db.cursor(dictionary=True)

        # Ajouter la recette avec la quantité
        cursor.execute("""
            INSERT INTO Recette (nom_recette, description) 
            VALUES (%(nom_recette)s, %(description)s);
        """, {"nom_recette": recette.nom_recette, "description": recette.description})

        recette_id = cursor.lastrowid

        # Ajouter les ingrédients à la recette
        for ingredient in recette.ingredients:
            # Ajouter l'ingrédient s'il n'existe pas
            cursor.execute("""
                INSERT INTO Ingredient (nom_ingredient) 
                VALUES (%(nom_ingredient)s)
                ON DUPLICATE KEY UPDATE nom_ingredient = nom_ingredient;
            """, {"nom_ingredient": ingredient.nom_ingredient})

            # Récupérer l'id de l'ingrédient
            cursor.execute("SELECT id FROM Ingredient WHERE nom_ingredient = %(nom_ingredient)s", {"nom_ingredient": ingredient.nom_ingredient})
            ingredient_id = cursor.fetchone()["id"]

            # Ajouter la relation Recette - Ingredient
            cursor.execute("""
                INSERT INTO RecetteIngredient (recette_id, ingredient_id, quantite) 
                VALUES (%(recette_id)s, %(ingredient_id)s, %(quantite)s);
            """, {"recette_id": recette_id, "ingredient_id": ingredient_id, "quantite": ingredient.quantite})

        # Committer les changements
        db.commit()

        return {"result": "Recette ajoutée", "nom_recette": recette.nom_recette, "ingredients": recette.ingredients, "description": recette.description}
    
    except Exception as e:
        print(f"Erreur lors de l'ajout de la recette : {e}")
        raise HTTPException(status_code=500, detail=f"Erreur interne du serveur : {e}")

    finally:
        # Fermer le curseur (le pool s'occupe de la connexion)
        cursor.close()

