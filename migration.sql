USE BDD_REC;

#INSERT INTO recettes(nom, Quantite, product_id) VALUES
    #('chocolat', 10, 'Chrismath cake'),
    #('jaunes doeufs', 5, 'Chrismath cake'),
    #('jambon', 2, 'Chrismath cake'),
    #('amande poudre', 10, 'Chrismath cake'),  -- Ajout de la virgule manquante ici
    #('noisettes hachees', 1, 'Chrismath cake'),  -- Ajout de la virgule manquante ici
    #('beurre', 5, 'Chrismath cake'),
    #('endive', 5, 'Chrismath cake'),
    #('canelle', 50, 'Chrismath cake');

#SELECT * FROM recettes;

-- Début de la transaction
#START TRANSACTION;

-- Insérer de nouvelles entrées
#INSERT INTO recettes (nom, Quantite, product_id) VALUES
    #('colorant bleu', 10, 'Tourte lagon'),
    #('orange', 5, 'Tourte lagon'),
    #('saumon', 2, 'Tourte lagon'),
    #('lait', 10, 'Tourte lagon'),
    #('jaune doeufs', 5, 'Tourte lagon'),
    #('cyanure', 5, 'Tourte lagon')
#ON DUPLICATE KEY UPDATE Quantite = VALUES(Quantite);

-- Mettre à jour une entrée existante
#UPDATE recettes SET Quantite = 20 WHERE nom = 'orange';

-- Valider la transaction
#COMMIT;

-- Sélectionner toutes les entrées
#SELECT * FROM recettes;



#INSERT INTO recettes (nom, Quantite, product_id) VALUES
    #('chapelure', 60, 'Green tea diet cake'),
    #('détergent', 1, 'Green tea diet cake'),
    #('vanille', 4, 'Green tea diet cake'),
   # ('cassonade', 20, 'Green tea diet cake'),
    #('colorant vert', 1, 'Green tea diet cake'),
   # ('anti-vomitif', 2, 'Green tea diet cake'),
    #('citronelle', 5, 'Green tea diet cake'),
   # ('sucre glace', 55, 'Green tea diet cake');

#SELECT * FROM recettes;


   #INSERT INTO recettes(nom, Quantite,product_id) VALUES
    #('sel', 40,'Grog de la victoire'),
    #('chocolat noir', 1,'Grog de la victoire'),
    #('zestes argrumes', 10,'Grog de la victoire'),
    #('levure chimique', 20,'Grog de la victoire'),
    #('cassonnade', 25,'Grog de la victoire'),
    #('parfum', 1,'Grog de la victoire');
    

    #SELECT * FROM recettes;

#INSERT INTO recettes (nom, Quantite, product_id) VALUES
    #('Uranium 232', 1, 'Salade pripiat'),
    #('pain', 2, 'Salade pripiat'),
    #('vanilles', 4, 'Salade pripiat'),
    #('petit chocolat', 20, 'Salade pripiat'),
   #('miel', 1, 'Salade pripiat'),
    #('endives', 2, 'Salade pripiat'),
    #('saumons', 5, 'Salade pripiat'),
    #('canelle poudre', 55, 'Salade pripiat');

#SELECT * FROM recettes;


    #INSERT INTO recettes (nom, Quantite,product_id) VALUES ('dopamines', 2,'Chrismath cake');

    #SELECT * from recettes;
 
    