import pandas as pd
import mysql.connector

# Configurations de connexion à la base de données
db_config = {
    "host": "localhost",
    "user": "benjamin",
    "password": "coucou",
    "database": "BDD_REC"
}

# Connectez-vous à la base de données MySQL
try:
    cnx = mysql.connector.connect(**db_config)
    cursor = cnx.cursor()

    # Lisez le fichier CSV
    with open('/home/benjamin/recette/recettes.csv', 'r') as csv_file:
        csv_file_commands = csv_file.read().split(';')

    # Exécutez les commandes SQL
    for command in csv_file_commands:
        try:
            cursor.execute(command)
        except Exception as e:
            print(f"Erreur lors de l'exécution de la commande : {e}")

    # Exécutez une requête pour récupérer les données
    query = "SELECT * FROM recettes;"
    df = pd.read_sql(query, cnx)

    # Affichez le contenu du DataFrame
    print("Contenu du DataFrame :")
    print(df)

    # Spécifiez le chemin pour le nouveau fichier CSV
    nouveau_csv_file_path = '/home/benjamin/recette/updaterecettes.csv'

    # Exportez le DataFrame vers un nouveau fichier CSV
    df.to_csv(nouveau_csv_file_path, index=False)

    print(f"\nLes données ont été exportées vers {nouveau_csv_file_path}")

except mysql.connector.Error as err:
    print(f"Erreur de connexion à la base de données : {err}")

finally:
    # Fermez la connexion à la base de données
    if 'cnx' in locals() and cnx.is_connected():
        cursor.close()
        cnx.close()


